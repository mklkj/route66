<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2017 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

abstract class Route66Rule
{
    protected $variables;
    protected $pattern;
    protected $tokens = array();
    private $regex = '';
    private $length = 0;

    public function __construct($pattern)
    {
        $this->pattern = $pattern;
        $this->regex = '/'.addcslashes(preg_replace('/{(.*?)}/', '(.*?)', $this->pattern), '/').'$/';
        preg_match_all('/{(.*?)}/', $this->pattern, $matches, PREG_SET_ORDER);
        if (is_array($matches) && count($matches)) {
            foreach ($matches as $match) {
                $this->tokens[] = '{'.$match[1].'}';
            }
        }
        $this->length = substr_count($this->pattern, '/');
    }

    abstract public function getTokensValues($query);

    abstract public function getQueryValue($key, $tokens);

    abstract public function getItemid($variables);

    public function getRouteVariables($filter = '')
    {
        if ($filter == 'conditions') {
            return array_filter($this->variables, array($this, 'conditionsFilter'));
        } elseif ($filter == 'menu') {
            return array_keys(array_filter($this->variables));
        } else {
            return $this->variables;
        }
    }

    protected function conditionsFilter($var)
    {
        return $var && $var != '@';
    }

    public function getOption()
    {
        return $this->variables['option'];
    }

    public function getPattern()
    {
        return $this->pattern;
    }

    public function getTokens()
    {
        return $this->tokens;
    }

    public function getRegex()
    {
        return $this->regex;
    }

    public function getLength()
    {
        return $this->length;
    }
}
