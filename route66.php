<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2017 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

class plgSystemRoute66 extends JPlugin
{
    protected static $language;

    public function onAfterInitialise()
    {
        $application = JFactory::getApplication();
        if ($application->isSite() && $application->getCfg('sef')) {

            // Set language
            self::setLanguage();

            // Initialize our router
            include_once JPATH_SITE.'/plugins/system/route66/lib/router.php';
            $Route66 = new Route66SefRouter();

            // Attach router callbacks
            $router = $application->getRouter();
            $router->attachBuildRule(array($Route66, 'preBuildRoute'), 'preprocess');
            $router->attachBuildRule(array($Route66, 'postBuildRoute'), 'postprocess');
            $router->attachParseRule(array($Route66, 'preParseRoute'), 'preprocess');
            $router->attachParseRule(array($Route66, 'postParseRoute'), 'postprocess');
        }
    }

    public function onAfterDispatch()
    {
        $application = JFactory::getApplication();
        if ($application->isSite() && $application->getCfg('sef')) {

            // Get document
            $document = JFactory::getDocument();

            // Duplicate URLs handling
            if ($application->input->getMethod() == 'GET' && ($this->params->get('canonical', 1) || $this->params->get('redirect', 1)) && $document->getType() == 'html') {

                // Get router
                $router = $application->getRouter();

                // Get URI
                $uri = JUri::getInstance();

                // Merge all the available vars
                $vars = array_merge($router->getVars(), $uri->getQuery(true));
                $vars = array_filter($vars);

                // Canonical URI
                $port = '';
                if (80 !== $uri->getPort()) {
                    $port = ':'.$_SERVER['SERVER_PORT'];
                }
                $canonical = $uri->getScheme().'://'.$uri->getHost().$port.JRoute::_('index.php?'.http_build_query($vars), false);

                // Current URI
                $current = $uri->toString();

                // Canonical and current URI should match. If not redirect
                if ($this->params->get('redirect', 1) && $current != JUri::root() && urldecode($current) != urldecode($canonical)) {

                    // Redirect
                    $application->redirect($canonical, 301);
                }

                // Add a canonical link. Exclude paginated pages and homepage
                if ($this->params->get('canonical', 1) && $current != JUri::root()) {
                    $canonical = JUri::getInstance($canonical);
                    $query = $canonical->getQuery(true);
                    if (!isset($query['start'])) {
                        $document->addHeadLink(htmlspecialchars($canonical->toString(array('scheme', 'host', 'port', 'path'))), 'canonical');
                    }
                }
            }
        }
    }

    public function onBeforeCompileHead()
    {
        $application = JFactory::getApplication();
        $document = JFactory::getDocument();
        if ($application->isSite() && $document->getType() == 'html') {
            $params = JComponentHelper::getParams('com_route66');
            if ($params->get('facebookPageId')) {
                $document->addCustomTag('<meta property="fb:pages" content="'.$params->get('facebookPageId').'" />');
            }
        }
    }

    protected static function setLanguage()
    {
        // Initialize object with default values
        $language = new stdClass();
        $language->code = '*';
        $language->sef = '';

        // Get application
        $application = JFactory::getApplication();

        // Proceed only if language filter is enabled
        if ($application->isSite() && $application->getLanguageFilter()) {

            // Get default site language
            $default = JComponentHelper::getParams('com_languages')->get('site', 'en-GB');

            // Get avaliable languages by SEF key
            $languages = JLanguageHelper::getLanguages('sef');

            // Do not rely on JFactory::getLanguage()->getTag() because it has not set yet if there is a language change in front-end
            $path = substr(JUri::current(), strlen(JUri::root(false)));
            if (strpos($path, 'index.php') === 0) {
                $path = substr($path, 9);
            }
            if (strlen($path) > 1 && strpos($path, '/') === 0) {
                $path = substr($path, 1);
            }
            $parts = explode('/', $path);
            if ($parts[0] && isset($languages[$parts[0]])) {
                $current = $languages[$parts[0]]->lang_code;
            }

            // If no language is detected fallback to the site default
            if (!isset($current)) {
                $current = $default;
            }
            $language->code = $current;

            // Check for the option "Remove URL Language Code" of the "System - Language Filter" to find out if the core router will add the language prefix
            $languageFilterPlugin = JPluginHelper::getPlugin('system', 'languagefilter');
            $languageFilterPluginParams = new JRegistry($languageFilterPlugin->params);

            $languageFilterPluginPrefixStatus = (bool) ($current != $default || !$languageFilterPluginParams->get('remove_default_prefix'));
            if ($languageFilterPluginPrefixStatus) {
                $languages = JLanguageHelper::getLanguages('lang_code');
                if (isset($languages[$language->code])) {
                    $language->sef = $languages[$language->code]->sef;
                }
            }
        }

        self::$language = $language;
    }

    public static function getLanguage()
    {
        return self::$language;
    }
}
